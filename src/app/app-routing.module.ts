import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TodosListComponent } from './todos/todos-list/todos-list.component';
import { TodoDetailComponent } from './todos/todo-detail/todo-detail.component';
import { TodosFormComponent } from './todos/todos-form/todos-form.component';

const routes: Routes = [
  {
    path: '',
    component: TodosListComponent
  },
  {
    path: 'add',
    component: TodosFormComponent
  },
  {
    path: ':id',
    component: TodoDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
