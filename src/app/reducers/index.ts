import { ActionReducerMap } from '@ngrx/store';
import { todoReducer, TodoState } from '../todos/todos-entity.reducer';
import { InjectionToken } from '@angular/core';
import { TodoListEffects } from '../todos/todos-entity.effects';

export interface State {
  todos: TodoState;
}

export const reducers: ActionReducerMap<State> = {
  todos: todoReducer
};

export function getReducers() {
  return reducers;
}

export const REDUCER_TOKEN = new InjectionToken<ActionReducerMap<State>>('Registered Reducers');

export  const  appEffects = [TodoListEffects];
