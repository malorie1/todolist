export interface TodosEntity {
  id: string;
  done: boolean;
  title: string;
  description?: string;
}
