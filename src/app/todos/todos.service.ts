import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TodosEntity } from './todos-entity.model';
import { HttpClient } from '@angular/common/http';
import { map, switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TodosService {

  constructor(private httpClient: HttpClient) { }

  getTodo(): Observable<TodosEntity[]> {
    return this.httpClient.get<TodosEntity[]>('/todos');
  }
  updateTodo(todo: TodosEntity): Observable<TodosEntity> {
    return this.httpClient.put<TodosEntity>(`/todos/${todo.id}`, todo);
  }

  getTodoById(id: string): Observable<TodosEntity> {
    return this.httpClient.get<TodosEntity>(`/todos/${id}`);
  }

  addTodo(todo: TodosEntity): Observable<TodosEntity> {
    return this.httpClient.post<TodosEntity>('/todos', todo);
  }
}
