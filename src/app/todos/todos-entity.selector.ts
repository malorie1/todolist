import * as fromTodos from './todos-entity.reducer';
import {createSelector} from '@ngrx/store';
import { State } from '../reducers';

export const selectTodoListState$ = (state: State) =>  state.todos;

export const selectTodoListEntitiesConverted$ = createSelector(
  selectTodoListState$,
  fromTodos.selectTodosEntities
);

export const selectTodoById = (id: string) => createSelector(
  selectTodoListState$,
  state => state.entities[id]
);
