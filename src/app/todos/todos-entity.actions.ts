import { Action } from '@ngrx/store';
import { TodosEntity } from './todos-entity.model';

export enum TodosEntityActionTypes {
  LoadTodosEntitys = '[TodosEntity] Load TodosEntitys',
  SuccessLoadTodosEntitys = '[TodosEntity] Success Load TodosEntitys',
  LoadTodosEntity = '[TodosEntity] Load One TodosEntity',
  SuccessLoadTodosEntity = '[TodosEntity] Success Load One TodosEntity',
  AddTodosEntity = '[TodosEntity] Add TodosEntity',
  ToggleTodoEntity = '[TodosEntity] Toggle TodosEntity'
}

export class LoadTodosEntitys implements Action {
  readonly type = TodosEntityActionTypes.LoadTodosEntitys;
}

export class LoadTodosEntity implements Action {
  readonly type = TodosEntityActionTypes.LoadTodosEntity;
  constructor(public id: string) {}
}

export class SuccessLoadTodosEntitys implements Action {
  readonly type = TodosEntityActionTypes.SuccessLoadTodosEntitys;
  constructor(public payload: TodosEntity[]) { }
}

export class ToggleTodoEntity implements Action {
  readonly type = TodosEntityActionTypes.ToggleTodoEntity;
  constructor(public payload: TodosEntity) { }
}

export class SuccessLoadTodosEntity implements Action {
  readonly type = TodosEntityActionTypes.SuccessLoadTodosEntity;
  constructor(public todo: TodosEntity) { }
}

export class AddTodosEntity implements Action {
  readonly type = TodosEntityActionTypes.AddTodosEntity;

  constructor(public todosEntity: TodosEntity) { }
}

export type TodosEntityActions =
  LoadTodosEntitys |
  LoadTodosEntity |
  AddTodosEntity |
  SuccessLoadTodosEntitys|
  ToggleTodoEntity |
  SuccessLoadTodosEntity;
