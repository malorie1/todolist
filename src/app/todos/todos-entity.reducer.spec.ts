import { todoReducer, initialState } from './todos-entity.reducer';
import {
  LoadTodosEntity,
  AddTodosEntity,
  LoadTodosEntitys, SuccessLoadTodosEntity,
  SuccessLoadTodosEntitys,
  TodosEntityActions
} from './todos-entity.actions';
import { TodosEntity } from './todos-entity.model';
import { v4 as uuid } from 'uuid';

describe('TodosEntity Reducer', () => {
  const items: TodosEntity[] = [
    {
      id: uuid(),
      title: 'TODO 1',
      description: 'TODO 1 description',
      done: false
    },
    {
      id: uuid(),
      title: 'TODO 2',
      description: 'TODO 2 description',
      done: false
    },
    {
      id: uuid(),
      title: 'TODO 3',
      description: 'TODO 3 description',
      done: true
    }
  ];
  describe('LoadTodosEntitys action', () => {
    it('should return intial test with LoadTodosEntitys action', () => {
      const action: TodosEntityActions = new LoadTodosEntitys;
      const result = todoReducer(initialState, action);
      expect(result).not.toBeNull();
    });
  });

  describe('SuccessTodosEntitys action', () => {
    it('should have ids', () => {
      const action: TodosEntityActions = new SuccessLoadTodosEntitys(items);
      const result = todoReducer(initialState, action);
      const ids = [ items[0].id, items[1].id, items[2].id];
      for ( let i = 0; i < result.ids.length; i ++) {
        expect(result.ids[i]).toBe(ids[i]);
      }
    });
  });

  describe('LoadTodosEntity action', () => {
    it('should return intial test with LoadTodosEntity action', () => {
      const action: TodosEntityActions = new LoadTodosEntity(items[0].id);
      const result = todoReducer(initialState, action);
      expect(result).not.toBeNull();
    });
  });

  describe('SuccessTodosEntity action', () => {
    it('should have one id', () => {
      const action: TodosEntityActions = new SuccessLoadTodosEntity(items[0]);
      const result = todoReducer(initialState, action);
      expect(result.ids.length).toEqual(1);
    });
    it('should have same id', () => {
      const action: TodosEntityActions = new SuccessLoadTodosEntity(items[0]);
      const result = todoReducer(initialState, action);
      expect(result.ids[0]).toBe(items[0].id);
    });
  });

  describe('AddTodosEntity action', () => {
    it('should return intial test with LoadTodosEntitys action', () => {
      const todo: TodosEntity = {
        id: uuid(),
        title: 'todo title',
        description: 'todo description',
        done: false
      };
      const action: TodosEntityActions = new AddTodosEntity(todo);
      const result = todoReducer(initialState, action);
      expect(result).not.toBeNull();
    });
  });
});
