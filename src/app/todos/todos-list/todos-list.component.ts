import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { selectTodoListEntitiesConverted$ } from '../todos-entity.selector';
import { LoadTodosEntitys, ToggleTodoEntity } from '../todos-entity.actions';
import { TodosEntity } from '../todos-entity.model';
import { State } from '../../reducers';

@Component({
  selector: 'app-todos-list',
  templateUrl: './todos-list.component.html',
  styleUrls: ['./todos-list.component.scss']
})
export class TodosListComponent implements OnInit {

  public todos$: Observable<TodosEntity[]>;

  constructor(private router: Router, private store: Store<State>) {
    this.todos$ = store
      .pipe(select(selectTodoListEntitiesConverted$));
  }

  ngOnInit() {
    this.store.dispatch(new LoadTodosEntitys());
  }

  public toggleTodo(todo: TodosEntity) {
    todo.done = !todo.done;
    this.store.dispatch(new ToggleTodoEntity(todo));
  }

  public navigate(todo: TodosEntity) {
    this.router.navigate([`/${todo.id}`]);
  }

}
