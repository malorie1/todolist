import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodosListComponent } from './todos-list.component';
import { combineReducers, Store, StoreModule } from '@ngrx/store';
import { getReducers, State } from '../../reducers';
import {
  LoadTodosEntitys,
  SuccessLoadTodosEntitys
} from '../todos-entity.actions';
import { MatCheckboxModule, MatIconModule, MatListModule } from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';
import { TodosEntity } from '../todos-entity.model';
import { v4 as uuid } from 'uuid';

describe('TodosListComponent', () => {
  let component: TodosListComponent;
  let fixture: ComponentFixture<TodosListComponent>;
  let store: Store<State>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodosListComponent ],
      imports: [
        MatIconModule,
        RouterTestingModule,
        MatCheckboxModule,
        MatListModule,
        StoreModule.forRoot({
          ...getReducers(),
          feature: combineReducers(getReducers)
        })
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();

    fixture = TestBed.createComponent(TodosListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should dispatch an action to load data when loaded', () => {
    const action = new LoadTodosEntitys();

    expect(store.dispatch).toHaveBeenCalledWith(action);
  });

  it('should display a list of items after data is loaded', () => {
    const items: TodosEntity[] = [
      {
        id: uuid(),
        title: 'TODO 1',
        description: 'TODO 1 description',
        done: false
      },
      {
        id: uuid(),
        title: 'TODO 2',
        description: 'TODO 2 description',
        done: false
      },
      {
        id: uuid(),
        title: 'TODO 3',
        description: 'TODO 3 description',
        done: true
      }
    ];
    const actions = new SuccessLoadTodosEntitys(items);
    store.dispatch(actions);

    component.todos$.subscribe(data => {
      expect(data.length).toBe(items.length);
    });
  });

});
