import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';

import { v4 as uuid } from 'uuid';
import { TodosEntity } from '../todos-entity.model';

const items: TodosEntity[] = [
  {
    id: '1234',
    title: 'TODO 1',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque euismod, tortor vitae rhoncus laoreet, turpis ' +
      'eros tristique lectus, venenatis auctor felis quam vitae est. Pellentesque sit amet tempus ligula, ac tincidunt' +
      ' lorem. Ut nulla sem, tincidunt at turpis ac, interdum dapibus dui. In rhoncus odio orci, varius efficitur ipsum imperdiet' +
      ' non. Curabitur luctus in dui at imperdiet. Proin sed facilisis leo. Nam nulla eros, mollis aliquam lacus ut, ' +
      'tristique efficitur ligula. Maecenas non mauris sem. ' +
      'Sed tristique urna non molestie condimentum. Nulla eros quam, iaculis pharetra volutpat quis, ullamcorper et orci.',
    done: false
  },
  {
    id: '3456',
    title: 'TODO 2',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque euismod, tortor vitae rhoncus laoreet, turpis ' +
      'eros tristique lectus, venenatis auctor felis quam vitae est. Pellentesque sit amet tempus ligula, ac tincidunt' +
      ' lorem. Ut nulla sem, tincidunt at turpis ac, interdum dapibus dui. In rhoncus odio orci, varius efficitur ipsum imperdiet' +
      ' non. Curabitur luctus in dui at imperdiet. Proin sed facilisis leo. Nam nulla eros, mollis aliquam lacus ut, ' +
      'tristique efficitur ligula. Maecenas non mauris sem. ' +
      'Sed tristique urna non molestie condimentum. Nulla eros quam, iaculis pharetra volutpat quis, ullamcorper et orci.',
    done: false
  },
  {
    id: '5678',
    title: 'TODO 3',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque euismod, tortor vitae rhoncus laoreet, turpis ' +
      'eros tristique lectus, venenatis auctor felis quam vitae est. Pellentesque sit amet tempus ligula, ac tincidunt' +
      ' lorem. Ut nulla sem, tincidunt at turpis ac, interdum dapibus dui. In rhoncus odio orci, varius efficitur ipsum imperdiet' +
      ' non. Curabitur luctus in dui at imperdiet. Proin sed facilisis leo. Nam nulla eros, mollis aliquam lacus ut, ' +
      'tristique efficitur ligula. Maecenas non mauris sem. ' +
      'Sed tristique urna non molestie condimentum. Nulla eros quam, iaculis pharetra volutpat quis, ullamcorper et orci.',
    done: true
  }
];

@Injectable()
export class HttpBackendMock implements HttpHandler {

  handle(req: HttpRequest<TodosEntity>): Observable<HttpEvent<any>> {

    let body: any;
    if (/todos\/?.*$/.test(req.url)) {
      const id = this.getResourceId(req);
      switch (req.method) {
        case 'POST':
          const itemToCreate = req.body;
          itemToCreate.id = (Number(items[items.length - 1].id) * 2).toString();
          items.unshift(itemToCreate);
          body = items;
          break;
        case 'PUT':
          if (id) {
            const itemToUpdate = req.body;
            const index = items.findIndex(item => item.id === id);
            items[index] = itemToUpdate;
            body = itemToUpdate;
          }
          break;
        case 'GET':
        default:
          if (id) {
            body = items.find(item => item.id === id);
          } else {
            body = items;
          }
      }
    }
    if (body) {
      return of(new HttpResponse({ body }));
    }
    return throwError(new HttpErrorResponse({
      status: 404
    }));
  }

  private getResourceId(req: HttpRequest<any>) {
    const [, id = null] = req.url.match(/todos\/(.+)\/?$/) || [];
    return id;
  }

}
