import { TodosService } from '../todos.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { HttpXhrBackend } from '@angular/common/http';
import { HttpBackendMock } from './http-backend.mock';

describe('HttpBackend Mock', () => {
  let todoService: TodosService;
  let http: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        TodosService,
        {
          provide: HttpXhrBackend,
          useClass: HttpBackendMock
        }
      ],
    });

    todoService = TestBed.get(TodosService);
    http = TestBed.get(HttpTestingController);
  });

  it('should request not be null', () => {
    todoService.getTodo().subscribe(response => {
      expect(response).toBeTruthy();
    });

    const httpRequest = http.expectOne('/todos');
    expect(httpRequest.request).not.toBeNull();
  });
});
