import { selectTodoListEntitiesConverted$, selectTodoListState$ } from './todos-entity.selector';

describe('Todo Entity Selector', () => {
  it('should selectTodoListEntitiesConverted$ not null', () => {
    expect(selectTodoListEntitiesConverted$).not.toBeNull();
  });
  it('should selectTodoListState$ not null', () => {
    expect(selectTodoListState$).not.toBeNull();
  });
});
