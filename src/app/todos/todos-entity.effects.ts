import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/internal/operators/switchMap';
import {
  AddTodosEntity,
  LoadTodosEntitys,
  LoadTodosEntity,
  SuccessLoadTodosEntity,
  SuccessLoadTodosEntitys,
  ToggleTodoEntity,
  TodosEntityActions,
  TodosEntityActionTypes
} from './todos-entity.actions';

import { map, throttleTime } from 'rxjs/operators';
import { TodosService } from './todos.service';
import { ofType } from '@ngrx/effects';
import { TodosEntity } from './todos-entity.model';

@Injectable()
export class TodoListEffects {
  @Effect() LoadTodos$: Observable<TodosEntityActions> = this.actions$
    .pipe(
      ofType(TodosEntityActionTypes.LoadTodosEntitys),
      switchMap(action => this.todoListService.getTodo()),
      map(todos => new SuccessLoadTodosEntitys(todos))
    );

  @Effect() LoadTodo$: Observable<TodosEntityActions> = this.actions$
    .pipe(
      ofType(TodosEntityActionTypes.LoadTodosEntity),
      switchMap((action: LoadTodosEntity) => this.todoListService.getTodoById(action.id)),
      map((todo: TodosEntity) => new SuccessLoadTodosEntity(todo))
    );

  @Effect() ToggleTodo$: Observable<TodosEntityActions> = this.actions$
    .pipe(
      throttleTime(1000),
      ofType(TodosEntityActionTypes.ToggleTodoEntity),
      switchMap((action: ToggleTodoEntity) => this.todoListService.updateTodo(action.payload)),
      map(todos => new ToggleTodoEntity(todos))
    );

  @Effect() AddTodo$: Observable<TodosEntityActions> = this.actions$
    .pipe(
      throttleTime(1000),
      ofType(TodosEntityActionTypes.AddTodosEntity),
      switchMap((action: AddTodosEntity) => this.todoListService.addTodo(action.todosEntity)),
      map(todos => new LoadTodosEntitys())
    );


  constructor(
    private todoListService: TodosService,
    private actions$: Actions,
  ) {
  }
}
