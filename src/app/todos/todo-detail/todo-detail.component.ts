import { Component, OnInit } from '@angular/core';
import { TodosEntity } from '../todos-entity.model';
import { ActivatedRoute, Params } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { State } from '../../reducers';
import { Observable } from 'rxjs';
import { selectTodoById } from '../todos-entity.selector';
import { LoadTodosEntity } from '../todos-entity.actions';

@Component({
  selector: 'app-todo-detail',
  templateUrl: './todo-detail.component.html',
  styleUrls: ['./todo-detail.component.scss']
})
export class TodoDetailComponent implements OnInit {

  todo$: Observable<TodosEntity>;

  constructor(private activatedRoute: ActivatedRoute, private store: Store<State>) {
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((value: Params) => {
      this.todo$ = this.store
        .pipe(select(selectTodoById(value.id)));

      this.store.dispatch(new LoadTodosEntity(value.id));
    });

  }
}
