import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoDetailComponent } from './todo-detail.component';
import { MatCardModule, MatChipsModule } from '@angular/material';
import { combineReducers, select, Store, StoreModule } from '@ngrx/store';
import { getReducers, State } from '../../reducers';
import {
  LoadTodosEntity,
  SuccessLoadTodosEntitys
} from '../todos-entity.actions';
import { TodosEntity } from '../todos-entity.model';
import { v4 as uuid } from 'uuid';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs/internal/observable/of';
import { selectTodoById } from '../todos-entity.selector';
import { RouterTestingModule } from '@angular/router/testing';

describe('TodoDetailComponent', () => {
  let component: TodoDetailComponent;
  let fixture: ComponentFixture<TodoDetailComponent>;
  let store: Store<State>;
  let items: TodosEntity[];
  let activatedRoute: ActivatedRoute;

  beforeEach(async(() => {
    items = [
      {
        id: uuid(),
        title: 'TODO 1',
        description: 'TODO 1 description',
        done: false
      },
      {
        id: uuid(),
        title: 'TODO 2',
        description: 'TODO 2 description',
        done: false
      },
      {
        id: uuid(),
        title: 'TODO 3',
        description: 'TODO 3 description',
        done: true
      }
    ];

    TestBed.configureTestingModule({
      declarations: [ TodoDetailComponent ],
      imports: [
        MatCardModule,
        RouterTestingModule,
        MatChipsModule,
        StoreModule.forRoot({
          ...getReducers(),
          feature: combineReducers(getReducers)
        })
      ],
      providers: [
        {
          provide: ActivatedRoute, useValue: {
            params: items[0].id
          }
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();


    fixture = TestBed.createComponent(TodoDetailComponent);
    component = fixture.componentInstance;

    activatedRoute = TestBed.get(ActivatedRoute);
    activatedRoute.params = of({ id: items[0].id });

    component.ngOnInit();
    fixture.detectChanges();
  });

  beforeEach(() => {
    const actions = new SuccessLoadTodosEntitys(items);
    store.dispatch(actions);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load one data', () => {
    const action = new LoadTodosEntity(items[0].id);
    store.dispatch(action);
    component.todo$ = store.pipe(select(selectTodoById(items[0].id)));
  });
});
