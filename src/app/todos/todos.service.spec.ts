import { fakeAsync, TestBed, tick } from '@angular/core/testing';

import { TodosService } from './todos.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TodosEntity } from './todos-entity.model';
import { v4 as uuid } from 'uuid';
import { HttpXhrBackend } from '@angular/common/http';
import { HttpBackendMock } from './mock/http-backend.mock';

describe('TodosService', () => {
  let http: HttpTestingController;
  let todosService: TodosService;

  beforeEach(() => {
    TestBed.configureTestingModule({
        imports: [
          HttpClientTestingModule
        ],
        providers: [
          TodosService,
          {
            provide: HttpXhrBackend,
            useClass: HttpBackendMock
          }
        ]
    });
    todosService = TestBed.get(TodosService);
    http = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    const service: TodosService = TestBed.get(TodosService);
    expect(service).toBeTruthy();
  });

  it('should url be ok', () => {
    let actualdata: TodosEntity[] = null;
    todosService.getTodo().subscribe((data: TodosEntity[]) => {
      actualdata = data;
    });

    const httpReq = http.expectOne('/todos');
    expect(httpReq.request.url).toBe('/todos');
  });

  it('should add data', () => {
    const todoToAdd: TodosEntity = {
      id: uuid(),
      title: 'todo Title',
      description: 'todo Description',
      done: false
    };

    todosService.addTodo(todoToAdd).subscribe(data => {
      expect(data).toBe(todoToAdd);
    });
  });
});
