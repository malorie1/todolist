import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { TodosEntity } from './todos-entity.model';
import { TodosEntityActions, TodosEntityActionTypes } from './todos-entity.actions';

export interface TodoState extends EntityState<TodosEntity> {
  selectEntities: TodosEntity;
}

export const adapter: EntityAdapter<TodosEntity> = createEntityAdapter<TodosEntity>();

export const initialState: TodoState = adapter.getInitialState({
  selectEntities: undefined
});

export const {
  selectAll: selectTodosEntities
} = adapter.getSelectors();

export function todoReducer(
  state = initialState,
  action: TodosEntityActions
): TodoState {
  switch (action.type) {

    case TodosEntityActionTypes.LoadTodosEntitys: {
      return {
        ...state
      };
    }

    case TodosEntityActionTypes.SuccessLoadTodosEntitys: {
      return {
        ...adapter.addAll(action.payload, state)
      };
    }

    case TodosEntityActionTypes.LoadTodosEntity: {
      return {
        ...state
      };
    }

    case TodosEntityActionTypes.SuccessLoadTodosEntity: {
      return {
        ...adapter.addOne(action.todo, state)
      };
    }

    case TodosEntityActionTypes.AddTodosEntity: {
      return {
        ...state
      };
    }

    case TodosEntityActionTypes.ToggleTodoEntity: {
      return adapter.upsertOne(action.payload, state);
    }

    default: {
      return state;
    }
  }
}
