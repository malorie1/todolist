import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { combineReducers, Store, StoreModule } from '@ngrx/store';

import { v4 as uuid } from 'uuid';
import { getReducers, State } from '../../reducers';
import { TodosFormComponent } from './todos-form.component';
import { AddTodosEntity } from '../todos-entity.actions';
import { TodosEntity } from '../todos-entity.model';
import { RouterTestingModule } from '@angular/router/testing';

describe('TodosFormComponent', () => {
  let component: TodosFormComponent;
  let fixture: ComponentFixture<TodosFormComponent>;
  let store: Store<State>;
  const formBuilder: FormBuilder = new FormBuilder();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodosFormComponent ],
      imports: [
        BrowserAnimationsModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatCardModule,
        MatInputModule,
        StoreModule.forRoot({
          ...getReducers(),
          feature: combineReducers(getReducers)
        }),
        RouterTestingModule
      ],
      providers: [
        { provide: FormBuilder, useValue: formBuilder }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();

    fixture = TestBed.createComponent(TodosFormComponent);
    component = fixture.componentInstance;
    component.formGroup = formBuilder.group({
      title: null,
      description: null
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should title validator working', () => {
    component.formGroup.reset();
    expect(component.formGroup.valid).toBeFalsy();
  });

  it('should submit ', () => {
    component.formGroup.get('title').setValue('Add a Todo');
    component.formGroup.get('description').setValue('This is the description of a todo to add');

    const todo: TodosEntity = component.formGroup.getRawValue();
    todo.id = uuid();
    todo.done = false;

    store.dispatch(new AddTodosEntity(todo));

    expect(store.dispatch).toHaveBeenCalledWith(new AddTodosEntity(todo));
  });
});
