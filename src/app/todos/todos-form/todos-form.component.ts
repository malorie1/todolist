import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TodosEntity } from '../todos-entity.model';
import { v4 as uuid } from 'uuid';
import { Store } from '@ngrx/store';
import { State } from '../../reducers';
import { AddTodosEntity } from '../todos-entity.actions';
import { Router } from '@angular/router';

@Component({
  selector: 'app-todos-form',
  templateUrl: './todos-form.component.html',
  styleUrls: ['./todos-form.component.scss']
})
export class TodosFormComponent implements OnInit {

  formGroup: FormGroup;

  constructor(
    private fb: FormBuilder,
    private store: Store<State>,
    private router: Router
  ) { }

  ngOnInit() {
    this.initForm();
  }

  private initForm() {
    this.formGroup = this.fb.group({
      title: ['', Validators.required],
      description: ['']
    });
  }

  submit() {
    if (this.formGroup.valid) {
      const todo: TodosEntity = this.formGroup.getRawValue();
      todo.id = '' ;
      todo.done = false;
      this.store.dispatch(new AddTodosEntity(todo));
      this.router.navigate(['']);
    } else {
      this.formGroup.markAsTouched();
    }
  }

}
