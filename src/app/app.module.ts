import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { TodosListComponent } from './todos/todos-list/todos-list.component';
import {
  MatChipsModule,
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatIconModule, MatInputModule,
  MatListModule,
  MatSelectModule
} from '@angular/material';
import { AppRoutingModule } from './app-routing.module';
import { StoreModule } from '@ngrx/store';
import { appEffects, getReducers, REDUCER_TOKEN } from './reducers';
import { EffectsModule } from '@ngrx/effects';
import { HttpClientModule, HttpXhrBackend } from '@angular/common/http';
import { HttpBackendMock } from './todos/mock/http-backend.mock';
import { TodoDetailComponent } from './todos/todo-detail/todo-detail.component';
import { TodosFormComponent } from './todos/todos-form/todos-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    AppComponent,
    TodosListComponent,
    TodoDetailComponent,
    TodosFormComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatListModule,
    MatCheckboxModule,
    MatIconModule,
    MatCardModule,
    MatChipsModule,
    MatButtonModule,
    MatSelectModule,
    MatInputModule,
    StoreModule.forRoot(REDUCER_TOKEN),
    EffectsModule.forRoot(appEffects)
  ],
  providers: [
    {
      provide: REDUCER_TOKEN,
      useFactory: getReducers
    },  {
      provide: HttpXhrBackend,
      useClass: HttpBackendMock
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
